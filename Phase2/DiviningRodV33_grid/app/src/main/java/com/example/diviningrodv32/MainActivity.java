package com.example.diviningrodv32;


/*
Divining Rod Program Version 1_8

This program is designed to collect data from manually entered input or data provided from a .csv file

Main Activity serves as the home page for the program.  For phase 1 it consists of an input page, settings page, and a tutorial page
*/

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.File;

//main activity for implementing buttons
public class MainActivity extends AppCompatActivity {

    //this is the button that will lead the user to the input activity
    Button input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this line initializes the input button from the xml file
        input = (Button) findViewById(R.id.ToInput);

        //this line sets the listener.  whenever the input button is pressed the program will change the activity to the input activity
        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), input.class));
            }
        });
    }
}
