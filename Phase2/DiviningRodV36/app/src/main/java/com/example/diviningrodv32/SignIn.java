package com.example.diviningrodv32;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SignIn extends AppCompatActivity {
    DatabaseHelper db;
    EditText e1, e2;
    Button b1, b2;

    //firebase
    DatabaseReference reff;
    private ArrayList<String> myUsers = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        db = new DatabaseHelper(this);
        e1 = (EditText)findViewById(R.id.email);
        e2 = (EditText)findViewById(R.id.pass);
        b1 = (Button)findViewById(R.id.login);
        b2 = (Button)findViewById(R.id.register);
        //---------------------------------------------------------------
        //firebase
        reff = FirebaseDatabase.getInstance().getReference("Users");

        reff.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String value = dataSnapshot.getValue(String.class);
                myUsers.add(value);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //-----------------------------------------------------------------
        // The db.deleteAll() is added to remove all the data in the database in test case
        //db.deleteAll();

        // This function has to be updated so it checks if the user is an administrator or not.
        // As now, it can only check if the user is registered or not.
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = e1.getText().toString();
                String password = e2.getText().toString();
                //-----------------------------------------
                //firebase

                //------------------------------------------
                boolean chkemailpass = db.emailpassword(email, password);
                boolean chkadmin = db.chkadmin(email);

                // This if/else statement will be updated so it checks administrator/regular user
                // Then it'd move to the main page rather than just prints a message.
                if ( chkemailpass == true ) {
                    if ( chkadmin == true ) {
                        Toast.makeText(getApplicationContext(), "Successfully Login as Admin", Toast.LENGTH_SHORT).show();
                        Intent ad = new Intent(getApplicationContext(), HomePage.class);
                        startActivity(ad);
                    } else {
                        Toast.makeText(getApplicationContext(), "Successfully Login as User", Toast.LENGTH_SHORT).show();
                        Intent user = new Intent(getApplicationContext(), HomePage.class);
                        startActivity(user);
                    }
                }
                else
                    Toast.makeText(getApplicationContext(), "Wrong email or password", Toast.LENGTH_SHORT).show();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Registering.class);
                startActivity(i);
            }
        });
    }
}
