package com.example.diviningrodv32;


/*
Divining Rod Program Version 1_8

This program is designed to collect data from manually entered input or data provided from a .csv file

Main Activity serves as the home page for the program.  For phase 1 it consists of an input page, settings page, and a tutorial page
*/

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

//main activity for implementing buttons
public class HomePage extends AppCompatActivity {

    //this is the button that will lead the user to the input activity
    Button input;

    Button signOut;

    Button tutorial;

    Button aboutThis;

    private static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        String[] permission =
                {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permission[0]) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permission[0]) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(HomePage.this, permission, REQUEST_CODE);
        }
        //this line initializes the input button from the xml file
        input = (Button) findViewById(R.id.ToInput);

        signOut = (Button) findViewById(R.id.exitLogin);

        tutorial = (Button) findViewById(R.id.ToTutorial);

        aboutThis = (Button) findViewById(R.id.ToAbout);

        //this line sets the listener.  whenever the input button is pressed the program will change the activity to the input activity
        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), input.class));
            }
        });

        tutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), tutorial.class));
            }
        });

        //sign out function. If yes, signs out the user. If no, does nothing
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(HomePage.this, R.style.AlertDialog).setTitle(R.string.title_signout)
                        .setMessage(R.string.message_signout)
                        .setPositiveButton(R.string.option_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(getApplicationContext(), SignIn.class));
                                Toast.makeText(HomePage.this, "Signed Out", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(R.string.option_no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();

            }
        });

        aboutThis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), about.class));
            }
        });
    }
}
