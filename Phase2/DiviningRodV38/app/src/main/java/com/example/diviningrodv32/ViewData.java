package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ViewData extends AppCompatActivity {

    List<Float> Arrdistance = new ArrayList<>();
    List<Float> Arrdepth = new ArrayList<>();
    List<Float> Arrresistance = new ArrayList<>();
    List<String> arr = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //these lines will recieve the array data from the input acvitiy and store them in duplicate arrays
        Intent intent = getIntent();

        Arrdistance = (ArrayList<Float>) intent.getSerializableExtra("distance");
        Arrdepth = (ArrayList<Float>) intent.getSerializableExtra("depth");
        Arrresistance = (ArrayList<Float>) intent.getSerializableExtra("resistance");

        //adds distance - depth - and resistance to arr rray
        for (int i = 0; i < Arrdepth.size(); i++) {
            arr.add(Float.toString(Arrdistance.get(i)) + " " + Float.toString(Arrdepth.get(i)) + " " + Float.toString(Arrresistance.get(i)));
        }

        //creates an adapter to load array in list view of the xml layout
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ViewData.this, android.R.layout.simple_list_item_1, arr);
        ListView Iv = (ListView) findViewById(R.id.Iv1);
        Iv.setAdapter(arrayAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), input.class));
        finish();
        return true;
    }

}
