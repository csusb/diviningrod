package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Registering extends AppCompatActivity {
    DatabaseHelper db;
    EditText re1, re2, re3;
    Button rb1;
    CheckBox rc1;

    private FirebaseAuth auth;

    //firebase
    DatabaseReference reff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registering);

        //firebase
        reff = FirebaseDatabase.getInstance().getReference("Users");

        db = new DatabaseHelper(this);
        re1 = (EditText)findViewById(R.id.register_email);
        re2 = (EditText)findViewById(R.id.register_pass);
        re3 = (EditText)findViewById(R.id.register_cpass);
        rb1 = (Button)findViewById(R.id.register_button);
        rc1 = (CheckBox)findViewById(R.id.register_checkbox);

        rb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1 = re1.getText().toString().replace(".", "*");
                String s2 = re2.getText().toString();
                String s3 = re3.getText().toString();
                if ( s1.equals("") || s2.equals("") || s3.equals("")) {
                    Toast.makeText(getApplicationContext(), "Fields are empty", Toast.LENGTH_SHORT).show();
                } else {
                    if ( s2.equals(s3) ) {
                        boolean chkmail = db.chkmail(s1);
                        if ( chkmail == true ) {
                            if ( rc1.isChecked() ) {
                                User user = new User(s1, s2, true);
                                boolean insert = db.insert(s1, s2, true);
                                //Adding them to firebase
                                reff.child(s1).setValue(user);
                                if ( insert == true ) {
                                    Toast.makeText(getApplicationContext(), "Successfully registered as an administrator", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                boolean insert = db.insert(s1, s2, false);
                                //Adding them to firebase
                                User user = new User(s1, s2, false);
                                reff.child(s1).setValue(user);
                                if ( insert == true ) {
                                    Toast.makeText(getApplicationContext(), "Successfully registered as an user", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Email already exists", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Password do not match", Toast.LENGTH_SHORT).show();
                    }
                }

                startActivity(new Intent(getApplicationContext(), SignIn.class));
            }
        });
    }
}

