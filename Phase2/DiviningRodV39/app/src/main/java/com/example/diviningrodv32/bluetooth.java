package com.example.diviningrodv32;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

public class bluetooth extends AppCompatActivity {
    Button getSignal;
    Button graph;
    private static final int REQUEST_CODE = 4;

    BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        getSignal = (Button) findViewById(R.id.obtainBTE);
        graph = (Button) findViewById(R.id.graphing);

        getSignal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BA == null){
                    Toast.makeText(bluetooth.this, "Bluetooth is not supported on this device", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(!BA.isEnabled()){
                        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQUEST_CODE);
                    }
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case REQUEST_CODE:
                if(resultCode == RESULT_OK){
                    Toast.makeText(this, "Bluetooth Activated", Toast.LENGTH_SHORT).show();
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}




