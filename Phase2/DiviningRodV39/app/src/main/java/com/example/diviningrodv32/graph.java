package com.example.diviningrodv32;

/*
Divining Rod Program Version 3_3

This program is designed to collect data from manually entered input or data provided from a .csv file

Graph serves as the graph creation page for the program.  For phase 1 it consists of the graph view and screenshot
*/

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import ca.hss.heatmaplib.HeatMap;
import ca.hss.heatmaplib.HeatMapMarkerCallback;


import com.example.diviningrodv32.Helper.FileUtil;
import com.example.diviningrodv32.Helper.ScreenshotUtil;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

public class graph extends AppCompatActivity {
    AppCompatActivity activity = graph.this;

    ScrollView ParentView;
    // this will be used to create the grid behind the heatmap
    LineChart chart;
    Bitmap bitmap;

    //this is for the screen shot -ivan
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        chart = findViewById(R.id.chart);
        ParentView = findViewById(R.id.ParentView);
        fab = findViewById(R.id.floatingActionButton);

        //these lines will recieve the array data from the input acvitiy and store them in duplicate arrays
        Intent intent = getIntent();
        ArrayList<Float> Arrdistance = (ArrayList<Float>) intent.getSerializableExtra("distance");
        ArrayList<Float> Arrdepth = (ArrayList<Float>) intent.getSerializableExtra("depth");
        ArrayList<Float> Arrresistance = (ArrayList<Float>) intent.getSerializableExtra("resistance");

        //this button initializes the array placeholder from the xml file
        HeatMap heatMap = (HeatMap) findViewById(R.id.heatmap);

        //these lines set boundaries and radius for the graph
        heatMap.setMinimum(0.0);
        heatMap.setMaximum(100.0);
        heatMap.setRadius(1000);



        //these next four lines define the color of the data points in the graph.
        //for this graph colors are defined as gradients.
        //The gtradient chosen is from green to blue as defined below
        Map<Float, Integer> colors = new ArrayMap<>();
        colors.put(0.0f, 0xFF00AF1F);
        colors.put(1.0f, 0xFF424BF4);
        heatMap.setColorStops(colors);



        chart.setBackgroundColor(Color.WHITE);
        chart.getDescription().setEnabled(false);

        XAxis xAxis;
        xAxis = chart.getXAxis();
        xAxis.enableAxisLineDashedLine(10f,10f,10f);
        xAxis.setAxisMinimum(0f);//sets the x axis min value
        xAxis.setAxisMaximum(26f);//sets the x axis max value
        xAxis.setLabelCount(7,true); // this will create x amount of lines between mina and max value

        YAxis yAxis;
        yAxis = chart.getAxisLeft();// set left axis
        yAxis.enableGridDashedLine(10f,10f,10f);
        yAxis.setAxisMinimum(-100f);// sets y axis min value
        yAxis.setAxisMaximum(0f);// sets y axis max value
        yAxis.setLabelCount(11,true);// this will create x amount of lines between min and max value
        chart.getAxisRight().setEnabled(false);// disables right axis values from appearing

        // the following lines of code are required to actually make the chart
        //appear if the chart has no data than no chart will be created
        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(0,0f));
        LineDataSet set = new LineDataSet(values,"data");
        set.setFillAlpha(110);
        ArrayList<ILineDataSet>dataSets = new ArrayList<>();
        dataSets.add(set);
        LineData data = new LineData(dataSets);
        chart.setData(data);

        //this code is to disable to the legend
        Legend legend = chart.getLegend();
        legend.setEnabled(false);



        //this statement will take information from the arrays recieved from the input activity and draw the graph using the data
        for (int i = 0; i < Arrdistance.size(); i++) {
            if(Arrresistance.get(i)*100 < 100 && Arrresistance.get(i)*100 > 10)
            {
                HeatMap.DataPoint point = new HeatMap.DataPoint((Arrdistance.get(i)/26), (Arrdepth.get(i)/100), Arrresistance.get(i)*100);
                heatMap.addData(point);
            }

        }

        //this line helps developers see what values are stored in the arrays and will not be visible by users.
        //it can be viewed from the logcat section or from the debugger section of android studio
        for( int i = 0; i < Arrdistance.size(); i++)
        {
            if(Arrdistance.get(i) != null || Arrdepth.get(i) != null || Arrresistance.get(i) != null)
            {
                System.out.println("\n" + Arrdistance.get(i) + Arrdepth.get(i) + Arrresistance.get(i) * 100);
            }
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                fab.setVisibility(View.GONE);
                bitmap = ScreenshotUtil.getInstance().takeScreenshotForScreen(activity);
                Date currentTime = Calendar.getInstance().getTime();
                String timestamp = currentTime.toString();
                String path = Environment.getExternalStorageDirectory().toString() + "/" + timestamp +"_GraphShot.png";
                FileUtil.getInstance().storeBitmap(bitmap,path);
                bitmap = null;
                fab.setVisibility(View.VISIBLE);

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), input.class));
        finish();
        return true;
    }
}
