package com.example.diviningrodv32;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class instruction2 extends AppCompatActivity {

    TextView firstPara;

    ImageView firstImg;

    ImageView secondImg;

    TextView secondPara;

    ImageView thirdImg;

    ImageView fourthImg;

    TextView thirdPara;

    ImageView fifthImg;

    ImageView sixthImg;

    TextView fourthPara;

    ImageView seventhImg;

    ImageView eightImg;

    TextView lastLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firstPara = (TextView) findViewById(R.id.firstPara);
        firstImg = (ImageView) findViewById(R.id.firstImg);
        secondImg = (ImageView) findViewById(R.id.secondImg);
        secondPara = (TextView) findViewById(R.id.secondParagraph);
        thirdImg = (ImageView) findViewById(R.id.thirdImg);
        fourthImg = (ImageView) findViewById(R.id.fourthImg);
        thirdPara = (TextView) findViewById(R.id.thirdPara);
        fifthImg = (ImageView) findViewById(R.id.fifthImg);
        sixthImg = (ImageView) findViewById(R.id.sixthImg);
        fourthPara = (TextView) findViewById(R.id.fourthPara);
        seventhImg = (ImageView) findViewById(R.id.seventhImg);
        eightImg = (ImageView) findViewById(R.id.eightImg);
        lastLine = (TextView) findViewById(R.id.lastLine);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
