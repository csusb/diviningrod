package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class about extends AppCompatActivity {
    TextView aboutThis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        aboutThis = (TextView) findViewById(R.id.aboutApp);
    }

    @Override
    public boolean onSupportNavigateUp() {
        Bundle extra = getIntent().getExtras();
        int data = extra.getInt("AdminMark");
        if(data == 1){
            startActivity(new Intent(about.this, HomePageAdmin.class));
        }
        else if (data == 0){
            startActivity(new Intent(about.this, HomePage.class));
        }
        finish();
        return true;
    }
}
