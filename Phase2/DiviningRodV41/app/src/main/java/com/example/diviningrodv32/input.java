package com.example.diviningrodv32;

/*
Divining Rod Program Version 3_9

This program is designed to collect data from manually entered input or data provided from a .csv file

Input serves as the data collection and importing activity for the program.  For phase 2 it consists of a manual input section, graph button, input tools, and a import csv button, bluetooth adapter button, view data button, and a clear data button
*/

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class input extends AppCompatActivity {
    private int STORAGE_PERMISSION = 25;
    //these three edit text lines initialize the field that takes data from the user
    //data includes distance, depth and resistance
    EditText distance;
    EditText depth;
    EditText resistance;

    //this button will collect the int values from the edit text fields
    Button collect;

    //this button will take the user to the graph page
    Button graph;
    //this button will import a csv file for the user to use
    Button importfile;

    //bluetooth
    Button btooth;

    //this button will open view data activity
    Button viewdata;
    //this button will clear all arrays
    Button clear;

    String path;



    private static final int READ_REQUEST_CODE = 1;
    //these arrays store the data entered by the user
    List<Float> Arrdistance = new ArrayList<>();
    List<Float> Arrdepth = new ArrayList<>();
    List<Float> Arrresistance = new ArrayList<>();

    //end of stuff
    private final static int REQUEST_ENABLE_BT = 1;//allows bluetooth to be enabled
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        /*if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            System.out.print("Device doesn't support Bluetooth");
        }
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }*/

        //these linse initializes the buttons and edit text fields from the xml file
        distance = (EditText) findViewById(R.id.distance);
        depth = (EditText) findViewById(R.id.depth);
        resistance = (EditText) findViewById(R.id.resistance);
        collect = (Button) findViewById(R.id.collect);
        graph = (Button) findViewById(R.id.ToGraph);
        viewdata = (Button) findViewById(R.id.Toviewdata);
        clear = (Button) findViewById(R.id.Toclear);
        importfile = (Button) findViewById(R.id.importfile);
        btooth = (Button) findViewById(R.id.bte);

        //this line sets the listener.  whenever the graph button is pressed the program will change the activity to the graph activity
        //the program will also collecet data stored in arrays and send it in packets to the graph activity
        //this will only happen if data is present in all three arrays and not null in any array
        graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if a data array is empty then the input page will refuse to let the user draw the graph as this could throw an error
                if( Arrdistance.size() == 0 || Arrresistance.size() == 0 || Arrdepth.size() == 0)
                {
                    System.out.println("must enter values\n" );
                    Toast.makeText(getApplicationContext(), "must enter values", Toast.LENGTH_SHORT).show();
                    return;
                }
                //if arrays have values then the arrays are sent to the graph activity and the activity is changed from input to graph
                else
                {
                    //this line prepares packets to be sent to the graph class
                    Intent intent=new Intent(getApplicationContext(), graph.class);

                    //these next three lines puts all three arrays in a bundle and sends them to the graph activity
                    intent.putExtra("distance", (ArrayList<Float>) Arrdistance);
                    intent.putExtra("depth", (ArrayList<Float>) Arrdepth);
                    intent.putExtra("resistance", (ArrayList<Float>) Arrresistance);

                    //this line  starts the graph activity specified above
                    startActivity(intent);
                }
            }
        });

        //this line sets the listener.  whenever the clear button is pressed the program will clear all arrays
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FuncClear();
            }
        });

        //this line sets the listener.  whenever the vie data button is pressed the program will change the activity to the view data activity
        //the program will also collecet data stored in arrays and send it in packets to the vie data activity
        //this will only happen if data is present in all three arrays and not null in any array
        viewdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if a data array is empty then the input page will refuse to let the user draw the graph as this could throw an error
                if( Arrdistance.size() == 0 || Arrresistance.size() == 0 || Arrdepth.size() == 0)
                {
                    System.out.println("must enter values\n" );
                    Toast.makeText(getApplicationContext(), "must enter values", Toast.LENGTH_SHORT).show();
                    return;
                }
                //if arrays have values then the arrays are sent to the graph activity and the activity is changed from input to graph
                else
                {
                    //this line prepares packets to be sent to the graph class
                    Intent intent=new Intent(getApplicationContext(), ViewData.class);

                    //these next three lines puts all three arrays in a bundle and sends them to the graph activity
                    intent.putExtra("distance", (ArrayList<Float>) Arrdistance);
                    intent.putExtra("depth", (ArrayList<Float>) Arrdepth);
                    intent.putExtra("resistance", (ArrayList<Float>) Arrresistance);

                    //this line  starts the view data activity specified above
                    startActivity(intent);
                }
            }
        });

        //this line sets the listener.  whenever the collect button is pressed the program will collect data input into the edit text fields and store them in their corresponding arrays
        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //these lines initiate float variables for temporary storage
                Float num1;
                Float num2;
                Float num3;

                //this statements will read if any of the three edit text fields are empty.  if they are then the program return and will not allow user to collect the data
                if (TextUtils.isEmpty(distance.getText().toString()) || TextUtils.isEmpty(depth.getText().toString())|| TextUtils.isEmpty(resistance.getText().toString())) {
                    System.out.println("must enter values\n" );

                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    Toast.makeText(getApplicationContext(), "must enter values", Toast.LENGTH_SHORT).show();
                    return;
                }

                //this saves the users inputed data for distance to the temporary variable num1.  this must be done so the program can make sure the data is in the accepted range
                //if it fails or is incompatible then the data is not saved
                try
                {
                    num1 = Float.parseFloat(distance.getText().toString());
                }
                catch (NumberFormatException ex)
                {
                    num1 = (float) 0;
                    Toast.makeText(input.this, "invalid text is present", Toast.LENGTH_LONG).show();
                }
                //this saves the users inputed data for depth to the temporary variable num2.  this must be done so the program can make sure the data is in the accepted range
                //if it fails or is incompatible then the data is not saved
                try
                {
                    num2 = Float.parseFloat(depth.getText().toString());
                }
                catch (NumberFormatException ex)
                {
                    num2 = (float) 0;
                    Toast.makeText(input.this, "invalid text is present", Toast.LENGTH_LONG).show();

                }
                //this saves the users inputed data for resistance to the temporary variable num3.  this must be done so the program can make sure the data is in the accepted range
                //if it fails or is incompatible then the data is not saved
                try
                {
                    num3 = Float.parseFloat(resistance.getText().toString());
                }
                catch (NumberFormatException ex)
                {
                    num3 = (float) 0;
                    Toast.makeText(input.this, "invalid text is present", Toast.LENGTH_LONG).show();
                }


                //this will check to see if the distance value was entered correctly.  If it was not then no input will be collected
                if(num1 > 26 || num1 <= 0)
                {
                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    System.out.println("must enter values between 0 and 26\n" );
                    Toast.makeText(getApplicationContext(), "must enter values between 0 and 26 for distance", Toast.LENGTH_LONG).show();
                    return;
                }

                //this will check to see if the depth value was entered correctly.  If it was not then no input will be collected
                else if(num2 > 100 || num2 <= 0)
                {
                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    System.out.println("must enter values between 0 and 100\n" );
                    Toast.makeText(getApplicationContext(), "must enter values between 0 and 100 for depth", Toast.LENGTH_LONG).show();
                    return;
                }

                //this will check to see if the resistance value was entered correctly.  If it was not then no input will be collected
                else if(num3 >=1 || num3 <= 0)
                {
                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    System.out.println("must enter values below 1 and above 0 for resistance\n" );
                    Toast.makeText(getApplicationContext(), "must enter values below 1 and above 0 for resistance", Toast.LENGTH_LONG).show();
                    return;
                }

                //If all conditions are satisfied then the program will save the corresponding data into their corresponding arrays
                else
                {
                    Arrdistance.add(num1);
                    Arrdepth.add(num2);
                    Arrresistance.add(num3);
                }
            }
        });

        //this sets the on click listener to open the file directory when the user wants to import a .csv file
        importfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //this line checks if user ahs allowed permission to access file storage.  if permission is not set then user will be asked to set it
                if(ContextCompat.checkSelfPermission
                        (input.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("text/*");
                    startActivityForResult(intent,READ_REQUEST_CODE );
                }
            }
        });

        btooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), bluetooth.class));
            }
        });

    }

    //when user has chosen the file they want to upload it will return the file path to the program and load it into the csv arrays
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode){
            case 1:
                if(resultCode == RESULT_OK){
                    path = data.getData().getPath();
                    String fileName = path.substring(path.lastIndexOf("/"));
                    String newPath = Environment.getExternalStorageDirectory().toString()+"/CSVFiles";
                    File directory = new File(newPath);
                    String mPath = directory.getAbsolutePath() + fileName;
                    addcsv(mPath);
                }
                break;
        }
    }
    //this function loads data from .csv files into arrays
    protected void addcsv(String mPath) {
        //initialize temp arrays for storing information
        ArrayList<String[]> records = new ArrayList<>();
        String[] record;
        ArrayList<Float> tempdistance = new ArrayList<>();
        ArrayList<Float> tempdepth = new ArrayList<>();
        ArrayList <Integer> templength = new ArrayList<>();
        boolean count = true;
        Float temp;
        //uses the file path to retrieve csv loaction and load that into a scanner
        File file = new File(mPath);
        try {
            System.out.println("trying \n");//debugging
            //sets the reader for the file
            BufferedReader br = new BufferedReader(new FileReader(file));
            String csvLine;
            //this loop will go through the entire csv file and store each data point into the arrays
            while ((csvLine = br.readLine()) != null) {
                //this will separate each entry by the , present in the csv file
                record = csvLine.split(",");
                templength.add(record.length);
                //this will not happen on the first iteration
                //this statement will store values for resistance and depth
                if (!count) {
                    for (int i = 1; i < record.length; i++) {
                        try
                        {
                            //this line will check if the csv data entry is within the correct range and can be covnerted to a double
                            //if it can not the prograsm wills top, clear the array, and inform the user of the error
                            temp = Float.parseFloat(record[i]);
                            if(temp > 1 || temp < 0)
                            {
                                Toast.makeText(input.this, "must enter values below 1 and above 0 for resistance in your csv" + temp + " is not allowed", Toast.LENGTH_LONG).show();
                                FuncClear();
                                return;
                            }
                            else
                            {
                                Arrresistance.add(temp);  //try to convert to float
                            }
                        }
                        catch (NumberFormatException ex)
                        {
                            Toast.makeText(input.this, "invalid text is present in the CSV file", Toast.LENGTH_LONG).show();
                            FuncClear();
                            return;
                        }
                    }
                    //this line will check if the csv data entry is within the correct range and can be covnerted to a double
                    //if it can not the prograsm wills top, clear the array, and inform the user of the error
                    try
                    {
                        temp = Float.parseFloat(record[0]);
                        tempdepth.add(temp);  //try to convert to float
                    }
                    catch (NumberFormatException ex)
                    {
                        Toast.makeText(input.this, "invalid text is present in the CSV file", Toast.LENGTH_LONG).show();
                        FuncClear();
                        return;
                    }
                }
                //this will only happen on the first iteration
                //this will store the value of distance
                if (count) {
                    for (int i = 1; i < record.length; i++) {
                        //this line will check if the csv data entry is within the correct range and can be covnerted to a double
                        //if it can not the prograsm wills top, clear the array, and inform the user of the error
                        try
                        {
                            temp = Float.parseFloat(record[i]);
                            tempdistance.add(temp);  //try to convert to float
                        }
                        catch (NumberFormatException ex)
                        {
                            Toast.makeText(input.this, "invalid text is present in the CSV file", Toast.LENGTH_LONG).show();
                            FuncClear();
                            return;
                        }
                    }
                    count = false;
                }
                //this saves all values for the csv array in temporary storage just in case
                records.add(record);
            }
            //this statement must be added for bug catching
            //because of the format of the previous loop the length of each line must be stored.
            //if the lengths are not equal that means a null value was stored in an array somewhere
            //the easiest way to solve this problem is by checking over the lengths of every line.
            // if there is one difference then the arrays must be cleared or a crash will occur
            for(int i = 0; i < templength.size(); i++)
            {
                if(templength.get(0) != templength.get(i))
                {
                    Toast.makeText(input.this, "invalid text is present in the CSV file", Toast.LENGTH_LONG).show();
                    FuncClear();
                    return;
                }
            }

            System.out.println("worked? \n");

            //this loop will iterate through our depth and distance temp arrays and add the correct nubmer of values into our primary arrays.
            //the loop will once again verify the range of values and then proceed to construct the completed algorithm
            for (int i = 0; i < tempdepth.size(); i++) {
                for (int k = 0; k < tempdistance.size(); k++) {

                    if(tempdistance.get(k) >26 || tempdistance.get(k) <0)
                    {
                        Toast.makeText(input.this, "must enter values below 26 and above 0 for distance in your csv" + tempdistance.get(k) + " is not allowed", Toast.LENGTH_LONG).show();
                        FuncClear();
                        return;
                    }
                    else
                    {
                        Arrdistance.add(tempdistance.get(k));//stores 14 values each time
                    }

                    if(tempdepth.get(i) >100 || tempdepth.get(i) <0)
                    {
                        Toast.makeText(input.this, "must enter values below 100 and above 0 for distance in your csv" + tempdepth.get(i) + " is not allowed", Toast.LENGTH_LONG).show();
                        FuncClear();
                        return;
                    }
                    else
                    {
                        Arrdepth.add(tempdepth.get(i));//happens 14 times
                    }
                }
            }
            //this line will cause the program to crash if incorrect values are entered
            //thus it is for debugging rather than programming purposes

            for (int i = 0; i < Arrdepth.size(); i++) { //prints arrays for debugging
                if (Arrdistance.get(i) != null || Arrdepth.get(i) != null || Arrresistance.get(i) != null) {
                    System.out.println("\n" + Arrdepth.get(i) + ", " + Arrdistance.get(i) + ", " + Arrresistance.get(i));
                }
            }

            //if a failure occured during the previous algorithm then these lines will run
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: " + ex);
        }
    }

    //Bluetooth socket code Connecting as Server
    /*private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            // Use a temporary object that is later assigned to mmServerSocket
            // because mmServerSocket is final.
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code.
                String NAME = "PairedDevice";
                tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME, UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
            } catch (IOException e) {
                Log.e("Catch error", "Socket's listen() method failed", e);
            }
            mmServerSocket = tmp;
        }
        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned.
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e("Catch error", "Socket's accept() method failed", e);
                    break;
                }
            }
        }
        // Closes the connect socket and causes the thread to finish.
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e("Catch error", "Could not close the connect socket", e);
            }
        }
    }

    //READING IN MESSAGES FROM BLUETOOTH
    public static class MyBluetoothService {
        private static final String TAG = "MY_APP_DEBUG_TAG";
        private Handler handler; // handler that gets info from Bluetooth service
        // Defines several constants used when transmitting messages between the
        // service and the UI.
        private interface MessageConstants {
            int MESSAGE_READ = 0;

        }
        private class ConnectedThread extends Thread {
            private final BluetoothSocket mmSocket;
            private final InputStream mmInStream;
            private byte[] mmBuffer; // mmBuffer store for the stream

            public ConnectedThread(BluetoothSocket socket) {
                mmSocket = socket;
                InputStream tmpIn = null;
                OutputStream tmpOut = null;

                // Get the input and output streams; using temp objects because
                // member streams are final.
                try {
                    tmpIn = socket.getInputStream();
                } catch (IOException e) {
                    Log.e(TAG, "Error occurred when creating input stream", e);
                }
                mmInStream = tmpIn;
            }
            public void run() {
                mmBuffer = new byte[1024];
                int numBytes; // bytes returned from read()

                // Keep listening to the InputStream until an exception occurs.
                while (true) {
                    try {
                        // Read from the InputStream.

                        numBytes = mmInStream.read(mmBuffer);
                        // Send the obtained bytes to the UI activity.
                        Message readMsg = handler.obtainMessage(MessageConstants.MESSAGE_READ, numBytes, -1,mmBuffer);
                        readMsg.sendToTarget();
                    } catch (IOException e) {
                        Log.d(TAG, "Input stream was disconnected", e);
                        break;
                    }
                }
            }
            // Call this method from the main activity to shut down the connection.
            public void cancel() {
                try {
                    mmSocket.close();
                } catch (IOException e) {
                    Log.e(TAG, "Could not close the connect socket", e);
                }
            }
        }
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        Bundle extra = getIntent().getExtras();
        int data = extra.getInt("AdminMark");
        if(data == 1){
            startActivity(new Intent(input.this, HomePageAdmin.class));
        }
        else if (data == 0){
            startActivity(new Intent(getApplicationContext(), HomePage.class));
        }
        finish();
        return true;
    }

    //this function clears our arrays in case of incorrect csv formatting
    protected void FuncClear() {
        Arrdistance.clear();
        Arrdepth.clear();
        Arrresistance.clear();
    }
}

