package com.example.diviningrodv32;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class instruction1 extends AppCompatActivity {

    //first body of text
    TextView partOne;

    //first image in
    ImageView firstImage;

    //second image in
    ImageView secondImage;

    //second body of text
    TextView partTwo;


    ImageView thirdImage;

    ImageView fourthImage;

    TextView lastSent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        partOne = (TextView) findViewById(R.id.firstPara);
        firstImage = (ImageView) findViewById(R.id.inputSpace);
        secondImage = (ImageView) findViewById(R.id.collectData);

        partTwo = (TextView) findViewById(R.id.secondPara);
        thirdImage = (ImageView) findViewById(R.id.thefinal);
        fourthImage = (ImageView) findViewById(R.id.goBack);
        lastSent = (TextView) findViewById(R.id.lastSent);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction1);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

