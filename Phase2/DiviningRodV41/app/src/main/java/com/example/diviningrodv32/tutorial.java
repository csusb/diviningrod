package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class tutorial extends AppCompatActivity {


    Button csvInstruction;
    Button manualInput;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        //Button calls for the tutorial
        manualInput = (Button) findViewById(R.id.instruction1);
        csvInstruction = (Button) findViewById(R.id.instruction2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //to tutorial on how to manually input data to test out app graph
        manualInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), instruction1.class));
            }
        });

        //to tutorial on how to use csv file to get data for graph
        csvInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), instruction2.class));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        Bundle extra = getIntent().getExtras();
        int data = extra.getInt("AdminMark");
        if(data == 1){
            startActivity(new Intent(tutorial.this, HomePageAdmin.class));
        }
        else if (data == 0){
            startActivity(new Intent(tutorial.this, HomePage.class));
        }
        finish();
        return true;
    }
}
