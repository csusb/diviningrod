package com.example.diviningrodv32;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, "Login.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table if not exists user(_id integer primary key, email text, password text, admin text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
    }

    //inserting in database
    public boolean insert(String email, String password, boolean admin) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("email", email);
        contentValues.put("password", password);
        contentValues.put("admin", admin);
        long ins = db.insert("user", null, contentValues);
        if ( ins == -1 ) return false;
        else return true;
    }

    //checking if email exists
    public boolean chkmail(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where email = ?", new String[] {email});
        if ( cursor.getCount() > 0 ) return false;
        else return true;
    }

    //checking the email and password
    public boolean emailpassword(String email, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where email = ? and password = ?", new String[] {email, password});
        if( cursor.getCount() > 0 ) return true;
        else return false;
    }

    //checking the administrator status
    public boolean chkadmin(String email) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where email = ? and admin = 1", new String[]{email});
        if (cursor.getCount() > 0) return true;
        else return false;
    }

    public Cursor viewData() {
        try {
            String selectQuery = "Select * from user order by _id desc";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            return cursor;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean chkchar(String entered_char){
        if(entered_char.contains("#") || entered_char.contains("$") || entered_char.contains("[") || entered_char.contains("]")){
            return false;
        }
        else{
            return true;
        }
    }

    public void deleteAll() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + "user");
        db.close();
    }

}
