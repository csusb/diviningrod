package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class blueTooth_Instruction_admin extends AppCompatActivity {

    TextView first_paragraph_bte;
    TextView second_paragraph_bte;
    TextView third_paragraph_bte;
    TextView fourth_paragraph_bte;



    ImageView imageOneB;
    ImageView imageTwoB;
    ImageView imageThreeB;
    ImageView imageFourB;
    ImageView imageFiveB;
    ImageView imageSixB;
    ImageView imageSevenB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blue_tooth__instruction_admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        first_paragraph_bte = (TextView) findViewById(R.id.firstPBTEA);
        second_paragraph_bte = (TextView) findViewById(R.id.secondPBTEA);
        third_paragraph_bte = (TextView) findViewById(R.id.thirdPBTEA);
        fourth_paragraph_bte = (TextView) findViewById(R.id.fourPBTEA);

        imageOneB = (ImageView) findViewById(R.id.firstIBTEA);
        imageTwoB = (ImageView) findViewById(R.id.secondIBTEA);
        imageThreeB = (ImageView) findViewById(R.id.thirdIBTEA);
        imageFourB = (ImageView) findViewById(R.id.fourIBTEA);
        imageFiveB = (ImageView) findViewById(R.id.fiveIBTEA);
        imageSixB = (ImageView) findViewById(R.id.sixIBTEA);
        imageSevenB = (ImageView) findViewById(R.id.sevenIBTEA);

    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial_admin.class));
        finish();
        return true;
    }
}