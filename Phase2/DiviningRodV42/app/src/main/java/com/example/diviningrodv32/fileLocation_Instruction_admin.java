package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class fileLocation_Instruction_admin extends AppCompatActivity {

    TextView descriptionFileLocation;

    ImageView fileLocationImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_location__instruction_admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descriptionFileLocation = (TextView) findViewById(R.id.descriptFLA);
        fileLocationImage = (ImageView) findViewById(R.id.imageFileLA);

    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial_admin.class));
        finish();
        return true;
    }
}
