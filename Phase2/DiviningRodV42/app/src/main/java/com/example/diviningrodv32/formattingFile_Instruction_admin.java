package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class formattingFile_Instruction_admin extends AppCompatActivity {
    TextView descriptionFileF;

    ImageView formatExample;

    TextView descriptionFile2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formating_file__instruction_admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descriptionFileF = (TextView) findViewById(R.id.descriptFFA);
        formatExample = (ImageView) findViewById(R.id.firstImageFFA);
        descriptionFile2 = (TextView) findViewById(R.id.descriptFF2A);

    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial_admin.class));
        finish();
        return true;
    }
}
