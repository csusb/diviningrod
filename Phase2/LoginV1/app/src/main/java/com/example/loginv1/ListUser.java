package com.example.loginv1;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;


public class ListUser extends AppCompatActivity {

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);

        db = new DatabaseHelper(this);


        populateListView();
    }

    /*
    public void FillList() {
        try {
            int[] id = {R.id.txtListElement};
            String[] email = new String[] {"email"};
            if (dbHelper == null)
                dbHelper = new DatabaseHelper(this);
            SQLiteDatabase sqlDb = dbHelper.getReadableDatabase();
            Cursor c = dbHelper.viewData();

            SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.list_template, c, email, id, 0);
            lstView.setAdapter(adapter);

        } catch (Exception ex) {
            Toast.makeText(ListUser.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }
    */

    private void populateListView() {

        Cursor cursor = db.viewData();
        String[] fromFieldName = new String[] {"_id", "email"};
        int[] toViewIDs = new int[] {R.id.textView_Email};
        SimpleCursorAdapter myCursorAdapter;
        myCursorAdapter = new SimpleCursorAdapter(getBaseContext(), R.layout.list_template, cursor, fromFieldName, toViewIDs, 0);
        ListView myList = (ListView) findViewById(R.id.listView_Users);
        myList.setAdapter(myCursorAdapter);

    }


}
