// This code is created by Hwanyi Lee
// This file is for login
// Check if an user is registered or not
// Check if an user is registered as an admin or not

package com.example.loginv1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DatabaseHelper db;
    EditText e1, e2;
    Button b1, b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DatabaseHelper(this);
        e1 = (EditText)findViewById(R.id.email);
        e2 = (EditText)findViewById(R.id.pass);
        b1 = (Button)findViewById(R.id.login);
        b2 = (Button)findViewById(R.id.register);

        // This function has to be updated so it checks if the user is an administrator or not.
        // As now, it can only check if the user is registered or not.
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = e1.getText().toString();
                String password = e2.getText().toString();

                boolean chkemailpass = db.emailpassword(email, password);
                boolean chkadmin = db.chkadmin(email);

                // This if/else statement will be updated so it checks administrator/regular user
                // Then it'd move to the main page rather than just prints a message.
                if ( chkemailpass == true ) {
                    if ( chkadmin == true ) {
                        Toast.makeText(getApplicationContext(), "Successfully Login as Admin", Toast.LENGTH_SHORT).show();
                        Intent ad = new Intent(MainActivity.this, MainPageForAd.class);
                        startActivity(ad);
                    } else {
                        Toast.makeText(getApplicationContext(), "Successfully Login as User", Toast.LENGTH_SHORT).show();
                        Intent user = new Intent(MainActivity.this, MainPageForUser.class);
                    }
                }
                else
                    Toast.makeText(getApplicationContext(), "Wrong email or password", Toast.LENGTH_SHORT).show();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Register.class);
                startActivity(i);
            }
        });
    }
}
