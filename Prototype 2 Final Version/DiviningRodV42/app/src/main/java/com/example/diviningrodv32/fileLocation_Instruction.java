package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class fileLocation_Instruction extends AppCompatActivity {

    TextView descriptionFileLocation;

    ImageView fileLocationImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_location_instruction);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descriptionFileLocation = (TextView) findViewById(R.id.descriptFL);
        fileLocationImage = (ImageView) findViewById(R.id.imageFileL);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial.class));
        finish();
        return true;
    }
}
