package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class homePage_Tutorial extends AppCompatActivity {

    TextView descriptionHomePage;

    ImageView mainHomePage;
    ImageView aboutMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_tutorial);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descriptionHomePage = (TextView) findViewById(R.id.descriptHP);
        mainHomePage = (ImageView) findViewById(R.id.firstImageHP);
        aboutMe = (ImageView) findViewById(R.id.secondImageHP);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial.class));
        finish();
        return true;
    }
}
