package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class login_tutorial extends AppCompatActivity {
    TextView loginDescription;
    ImageView loginPage;
    ImageView registerPage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_tutorial);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Purpose of this page is just make the layout for users who need to know how
        //to create an account or login
        loginDescription = (TextView) findViewById(R.id.loginHelp);
        loginPage = (ImageView) findViewById(R.id.imageLogin);
        registerPage = (ImageView) findViewById(R.id.imageRegister);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), SignIn.class));
        finish();
        return true;
    }
}
