package com.example.diviningrodv32;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class manualInput_Instruction_admin extends AppCompatActivity {

    //first body of text
    TextView partOne;

    //first image in
    ImageView firstImage;

    //second image in
    ImageView secondImage;

    //second body of text
    TextView partTwo;


    ImageView thirdImage;

    ImageView fourthImage;

    TextView lastSent;

    TextView finalLine;

    ImageView nineImage;

    ImageView tenImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_input__instruction_admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        partOne = (TextView) findViewById(R.id.firstPara);
        firstImage = (ImageView) findViewById(R.id.inputSpace);
        secondImage = (ImageView) findViewById(R.id.collectData);

        partTwo = (TextView) findViewById(R.id.secondPara);
        thirdImage = (ImageView) findViewById(R.id.thefinal);
        fourthImage = (ImageView) findViewById(R.id.goBack);
        lastSent = (TextView) findViewById(R.id.lastSent);

        finalLine = (TextView) findViewById(R.id.finalLine);
        nineImage = (ImageView) findViewById(R.id.view_data_button);
        tenImage = (ImageView) findViewById(R.id.view_data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial_admin.class));
        finish();
        return true;
    }
}