package com.example.diviningrodv32;

import java.util.ArrayList;
import java.util.List;

public class Arraykeeper {
    private static Arraykeeper instance;

    //constructors
    private List<Float> ArrDepth;
    private List<Float> ArrResistance;
    private List<Float> ArrDistance;
    private Arraykeeper(){
        ArrDepth = new ArrayList<>();
        ArrResistance = new ArrayList<>();
        ArrDistance = new ArrayList<>();
    }

    //adds individual values to array
    public void addDepth(Float num) {
        this.ArrDepth.add(num);
    }
    public void addDistacne(Float num) {
        this.ArrDistance.add(num);
    }
    public void addResistance(Float num) {
        this.ArrResistance.add(num);
    }

    //sets singleton arryas to array
    public void setDepth(List<Float> arr){
        this.ArrDepth = arr;
    }
    public void setDistance(List<Float> arr){
        this.ArrDistance = arr;
    }
    public void setResistance(List<Float> arr){
        this.ArrResistance = arr;
    }

    //returns the array
    public List<Float> getDepth(){
        return this.ArrDepth;
    }
    public List<Float> getDistance(){
        return this.ArrDistance;
    }
    public List<Float> getResistance(){
        return this.ArrResistance;
    }

    public void print(){
        for (int i = 0; i < ArrDepth.size(); i++) { //prints arrays for debugging
            if (ArrDistance.get(i) != null || ArrDepth.get(i) != null || ArrResistance.get(i) != null) {
                System.out.println("\n" + ArrDepth.get(i) + ", " + ArrDistance.get(i) + ", " + ArrResistance.get(i));
            }
        }
    }

    public void clear() {
        ArrDistance.clear();
        ArrDepth.clear();
        ArrResistance.clear();
    }


    //initialize
    public static synchronized Arraykeeper getInstance(){
        if(instance == null)
        {
            instance = new Arraykeeper();
        }
        return instance;
    }



}
