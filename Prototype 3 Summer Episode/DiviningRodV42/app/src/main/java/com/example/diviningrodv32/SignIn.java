package com.example.diviningrodv32;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.diviningrodv32.TutorialStuff.login_tutorial;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SignIn extends AppCompatActivity {
    ImageView logo;
    DatabaseHelper db;
    EditText e1, e2;
    Button b1, b2;

    Button loginTutorial;

    DatabaseReference reff;
    List<User> myUsers = new ArrayList<User>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Firebase Code
        //firebase
        reff = FirebaseDatabase.getInstance().getReference("Users");
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                try {
                    for (DataSnapshot item_snapshot : dataSnapshot.getChildren()) {
                        User newUser = item_snapshot.getValue(User.class);
                        String email = newUser.getEmail();
                        email = email.replace("*", ".");
                        boolean chkemailpass = db.emailpassword(email, newUser.getPassword());
                        if (chkemailpass == false) {
                            boolean insert = db.insert(email, newUser.getPassword(), newUser.getAdminstatus());
                            if (insert == true) {
                                //Toast.makeText(getApplicationContext(), "Successfully registered as an administrator", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }
                catch(Exception e){
                    //Toast.makeText(getApplicationContext(), " ", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("Firebase", "loadPost:onCancelled", databaseError.toException());
            }
        };
        reff.addValueEventListener(postListener);
        //----------------------------------------------------------------

        setContentView(R.layout.activity_sign_in);

        logo = (ImageView) findViewById(R.id.drLogo);
        db = new DatabaseHelper(this);
        e1 = (EditText) findViewById(R.id.email);
        e2 = (EditText) findViewById(R.id.pass);
        b1 = (Button) findViewById(R.id.login);
        b2 = (Button) findViewById(R.id.register);

        loginTutorial = (Button) findViewById(R.id.loginGuide);



        //Updating database through firebase array


        // This function has to be updated so it checks if the user is an administrator or not.
        // As now, it can only check if the user is registered or not.
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = e1.getText().toString().replace("*", ".");
                for(char c: email.toCharArray()){
                    if(c == '/'){
                        Toast.makeText(SignIn.this, "Bad Character", Toast.LENGTH_SHORT).show();
                    }
                }
                String password = e2.getText().toString();


                boolean chkemailpass = db.emailpassword(email, password);
                boolean chkadmin = db.chkadmin(email);

                // This if/else statement will be updated so it checks administrator/regular user
                // Then it'd move to the main page rather than just prints a message.
                if (chkemailpass == true) {
                    if (chkadmin == true) {
                        Toast.makeText(getApplicationContext(), "Successfully Login as Admin", Toast.LENGTH_SHORT).show();
                        Intent ad = new Intent(getApplicationContext(), HomePageAdmin.class);

                        startActivity(ad);
                    } else {
                        Toast.makeText(getApplicationContext(), "Successfully Login as User", Toast.LENGTH_SHORT).show();
                        Intent user = new Intent(getApplicationContext(), HomePage.class);
                        startActivity(user);
                    }
                } else
                    Toast.makeText(getApplicationContext(), "Wrong email or password", Toast.LENGTH_SHORT).show();
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Registering.class);
                startActivity(i);
            }
        });

        loginTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), login_tutorial.class));
            }
        });
    }
}
