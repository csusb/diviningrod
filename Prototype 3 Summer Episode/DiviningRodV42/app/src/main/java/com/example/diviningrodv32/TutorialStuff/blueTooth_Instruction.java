package com.example.diviningrodv32.TutorialStuff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diviningrodv32.R;
import com.example.diviningrodv32.tutorial;

public class blueTooth_Instruction extends AppCompatActivity {

    TextView first_paragraph_bte;
    TextView second_paragraph_bte;
    TextView third_paragraph_bte;
    TextView fourth_paragraph_bte;

    ImageView imageOneB;
    ImageView imageTwoB;
    ImageView imageThreeB;
    ImageView imageFourB;
    ImageView imageFiveB;
    ImageView imageSixB;
    ImageView imageSevenB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_instruction);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        first_paragraph_bte = (TextView) findViewById(R.id.firstPBTE);
        second_paragraph_bte = (TextView) findViewById(R.id.secondPBTE);
        third_paragraph_bte = (TextView) findViewById(R.id.thirdPBTE);
        fourth_paragraph_bte = (TextView) findViewById(R.id.fourPBTE);

        imageOneB = (ImageView) findViewById(R.id.firstIBTE);
        imageTwoB = (ImageView) findViewById(R.id.secondIBTE);
        imageThreeB = (ImageView) findViewById(R.id.thirdIBTE);
        imageFourB = (ImageView) findViewById(R.id.fourIBTE);
        imageFiveB = (ImageView) findViewById(R.id.fiveIBTE);
        imageSixB = (ImageView) findViewById(R.id.sixIBTE);
        imageSevenB = (ImageView) findViewById(R.id.sevenIBTE);

    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial.class));
        finish();
        return true;
    }
}
