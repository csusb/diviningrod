package com.example.diviningrodv32.TutorialStuff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diviningrodv32.R;

public class csvInstruction_Instruction_admin extends AppCompatActivity {

    TextView firstPara;

    ImageView firstImg;

    ImageView secondImg;

    TextView secondPara;

    ImageView thirdImg;

    ImageView fourthImg;

    TextView thirdPara;

    ImageView fifthImg;

    ImageView sixthImg;

    TextView fourthPara;

    ImageView seventhImg;

    ImageView eightImg;

    TextView lastLine;

    TextView finalLine;

    ImageView nineImage;

    ImageView tenImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_csv_instruction__instruction_admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firstPara = (TextView) findViewById(R.id.firstPara);
        firstImg = (ImageView) findViewById(R.id.firstImg);
        secondImg = (ImageView) findViewById(R.id.secondImg);
        secondPara = (TextView) findViewById(R.id.secondParagraph);
        thirdImg = (ImageView) findViewById(R.id.thirdImg);
        fourthImg = (ImageView) findViewById(R.id.fourthImg);
        thirdPara = (TextView) findViewById(R.id.thirdPara);
        fifthImg = (ImageView) findViewById(R.id.fifthImg);
        sixthImg = (ImageView) findViewById(R.id.sixthImg);
        fourthPara = (TextView) findViewById(R.id.fourthPara);
        seventhImg = (ImageView) findViewById(R.id.seventhImg);
        eightImg = (ImageView) findViewById(R.id.eightImg);
        lastLine = (TextView) findViewById(R.id.lastLine);
        finalLine = (TextView) findViewById(R.id.finalLine);
        nineImage = (ImageView) findViewById(R.id.view_data_button);
        tenImage = (ImageView) findViewById(R.id.view_data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial_admin.class));
        finish();
        return true;
    }
}