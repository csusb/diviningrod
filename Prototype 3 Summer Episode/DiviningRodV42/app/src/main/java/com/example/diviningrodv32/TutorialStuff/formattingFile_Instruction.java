package com.example.diviningrodv32.TutorialStuff;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diviningrodv32.R;
import com.example.diviningrodv32.tutorial;

import org.w3c.dom.Text;

public class formattingFile_Instruction extends AppCompatActivity {

    TextView descriptionFileF;

    ImageView formatExample;

    TextView descriptionFile2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formatting_file_instruction);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descriptionFileF = (TextView) findViewById(R.id.descriptFF);
        formatExample = (ImageView) findViewById(R.id.firstImageFF);
        descriptionFile2 = (TextView) findViewById(R.id.descriptFF2);

    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial.class));
        finish();
        return true;
    }
}
