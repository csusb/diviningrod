package com.example.diviningrodv32.TutorialStuff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diviningrodv32.R;

public class homePage_Tutorial_Admin extends AppCompatActivity {

    TextView descriptionHomePage;

    ImageView mainHomePage;
    ImageView aboutMe;

    TextView secondParagraphHP;
    ImageView usersButton;
    ImageView listUsers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page__tutorial__admin);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        descriptionHomePage = (TextView) findViewById(R.id.descriptHPA);
        mainHomePage = (ImageView) findViewById(R.id.firstImageHPA);
        aboutMe = (ImageView) findViewById(R.id.secondImageHPA);
        secondParagraphHP = (TextView) findViewById(R.id.secondPHPA);
        usersButton = (ImageView) findViewById(R.id.thirdImageHPA);
        listUsers = (ImageView) findViewById(R.id.fourthImageHPA);

    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), tutorial_admin.class));
        finish();
        return true;
    }
}
