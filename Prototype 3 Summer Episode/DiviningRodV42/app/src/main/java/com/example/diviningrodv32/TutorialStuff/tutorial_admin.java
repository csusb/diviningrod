package com.example.diviningrodv32.TutorialStuff;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.diviningrodv32.HomePageAdmin;
import com.example.diviningrodv32.R;

public class tutorial_admin extends AppCompatActivity {

    Button csvInstruction;
    Button manualInput;
    Button homePageLook;
    Button formattingFile;
    Button fileLocation;
    Button blueToothOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_admin);
        //Button calls for the tutorial
        manualInput = (Button) findViewById(R.id.instruction1);
        csvInstruction = (Button) findViewById(R.id.instruction2);
        homePageLook = (Button) findViewById(R.id.instruction3);
        formattingFile = (Button) findViewById(R.id.instruction4);
        fileLocation = (Button) findViewById(R.id.instruction5);
        blueToothOption = (Button) findViewById(R.id.instruction6);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //to tutorial on how to manually input data to test out app graph
        manualInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), manualInput_Instruction_admin.class));
            }
        });

        //to tutorial on how to use csv file to get data for graph
        csvInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), csvInstruction_Instruction_admin.class));
            }
        });

        //tutorial on how the home page looks
        homePageLook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), homePage_Tutorial_Admin.class));
            }
        });

        //tutorial how the csv files should look
        formattingFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), formattingFile_Instruction_admin.class));
            }
        });

        //where the files should be placed
        fileLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), fileLocation_Instruction_admin.class));
            }
        });

        //how the bluetooth works
        blueToothOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), blueTooth_Instruction_admin.class));
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), HomePageAdmin.class));
        finish();
        return true;
    }
}

