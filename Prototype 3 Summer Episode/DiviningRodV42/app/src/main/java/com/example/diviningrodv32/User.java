package com.example.diviningrodv32;

/*
This Class is used to store user information
in a single object so that User data can be
pushed to the Firebase Server as ont object.
*/

public class User {
    private String email;
    private String password;
    private boolean adminstatus;

    public User() {
    }


    public User(String em, String pass, boolean adst) {
        email = em;
        password = pass;
        adminstatus = adst;
    }
    public String getEmail(){return email;}
    public String getPassword(){return password;}
    public boolean getAdminstatus(){return adminstatus;}

    public void setEmail(String em){
        email = em;
    }
    public void setPassword(String pass){
        password = pass;
    }
    public void setAdmin(boolean stat){
        adminstatus = stat;
    }

}
