package com.example.diviningrodv32;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class bluetooth extends AppCompatActivity {
    Button getSignal;
    Button signalOff;
    Button discoverMe;
    Button pairingMe;

    TextView viewPaired;


    ImageView btLogo;

    private static final int REQUEST_CODE = 4;
    private static final int REQUEST_DISCOVER = 5;

    BluetoothAdapter BA = BluetoothAdapter.getDefaultAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSignal = (Button) findViewById(R.id.obtainBTE);
        signalOff = (Button) findViewById(R.id.disableBTE);
        discoverMe = (Button) findViewById(R.id.discoverBTE);
        pairingMe = (Button) findViewById(R.id.pairBTE);
        viewPaired = (TextView) findViewById(R.id.pairedBTE);
        btLogo = (ImageView) findViewById(R.id.logoBTE);

        if(!BA.isEnabled()) {
            btLogo.setImageResource(R.drawable.ic_action_null);
        }

        //Turns Bluetooth On
        getSignal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BA == null){
                    Toast.makeText(bluetooth.this, "Bluetooth is not supported on this device", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(!BA.isEnabled()){
                        startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), REQUEST_CODE);
                    }
                }
            }
        });


        //Turns Bluetooth Off
        signalOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BA.isEnabled()){
                    BA.disable();
                    btLogo.setImageResource(R.drawable.ic_action_off);
                    Toast.makeText(bluetooth.this, "Bluetooth Disabled", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(bluetooth.this, "Bluetooth Already Disabled", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //Allows device to be discovered by other devices
        discoverMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!BA.isDiscovering()){
                    Toast.makeText(bluetooth.this, "Making Device Discoverable", Toast.LENGTH_SHORT).show();
                    Intent btIntentDiscover = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    startActivityForResult(btIntentDiscover, REQUEST_DISCOVER);
                }
            }
        });


        //Shows all pairings that the device has made
        pairingMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(BA.isEnabled()){
                    viewPaired.setText("Paired Devices: ");
                    Set<BluetoothDevice> devices = BA.getBondedDevices();
                    for(BluetoothDevice device: devices){
                        viewPaired.append("\nDevice" + device.getName() + ", " + device);
                    }
                }
                else{
                    Toast.makeText(bluetooth.this, "Bluetooth is off. Must turn on to pair device", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //What happens when bluetooth is enabled
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode){
            case REQUEST_CODE:
                if(resultCode == RESULT_OK){
                    Toast.makeText(this, "Bluetooth Activated", Toast.LENGTH_SHORT).show();
                    btLogo.setImageResource(R.drawable.ic_action_on);
                }
                else{
                    Toast.makeText(this, "Error has occured", Toast.LENGTH_SHORT).show();
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), input.class));
        finish();
        return true;
    }
}




