package com.example.diviningrodv32;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class userList extends AppCompatActivity {

    DatabaseHelper db;
    ListView lstView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listusers);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        db = new DatabaseHelper(this);
        lstView = (ListView) findViewById(R.id.listView_Users);

        FillList();
    }

    public void FillList() {
        try {
            int[] id = {R.id.textView_Email};
            String[] email = new String[] {"email"};
            if (db == null)
                db = new DatabaseHelper(this);
            SQLiteDatabase sqlDb = db.getReadableDatabase();
            Cursor c = db.viewData();

            SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.list_template, c, email, id, 0);
            lstView.setAdapter(adapter);

        } catch (Exception ex) {
            Toast.makeText(userList.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(getApplicationContext(), HomePageAdmin.class));
        finish();
        return true;
    }

}
