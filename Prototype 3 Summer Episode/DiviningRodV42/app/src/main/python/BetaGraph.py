from android.os import Bundle
from android.os import Environment
from android.graphics import BitmapFactory
from android.support.v7.app import AppCompatActivity
from android.widget import Button
from android.view import View
from com.example.diviningrodv32 import R
from java import jvoid, Override, static_proxy
import numpy as np              #library for scientific computing
import matplotlib.pyplot as plt #pyplot library for plotting data
import matplotlib.image as mpimg
import csv
import datetime




from matplotlib import cm       #cm library for color mapping
import scipy.interpolate        #library in helping data interpolation

class MainActivity(static_proxy(AppCompatActivity, View.OnClickListener)):

    @Override(jvoid, [Bundle])
    def onCreate(self, state):
        AppCompatActivity.onCreate(self, state)
        self.setContentView(R.layout.activity_beta_graph)
        #initialize variable to handle data list
        x = []          #holds the values of X axis in data file
        y = []          #holds the values of Y axis in data file
        z = []          #holds the values of Z axis in data file
        root = Environment.getExternalStorageDirectory()
        #get data from files
        with open(root.getAbsolutePath() + '/TestFiles/out.csv','r') as csvfile:
            plots = csv.reader(csvfile, delimiter=',')
            for row in plots:
                # append data from read line (in row number) to corresponding (X,Y,Z) coordinates
                x.append(float(row[0]))
                y.append(float(row[1]))
                z.append(float(row[2]))
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        xi, yi = np.linspace(x.min(), x.max(), 500), np.linspace(y.min(), y.max(), 500)
        xi, yi = np.meshgrid(xi, yi)
        rbf = scipy.interpolate.Rbf(x, y, z, function='linear')
        zi = rbf(xi, yi)
        plt.figure(figsize=(26,26))
        norm = cm.colors.Normalize(vmax= z.max(), vmin=-z.max())
        plt.contour(xi, yi, zi, 15, linewidths = 0.2, rasterized= True,antialiased = True, norm= norm, colors ='k')
        plt.pcolormesh(xi, yi, zi, cmap=plt.cm.jet) #rainbow color
        plt.grid(color='w', which= 'both', linestyle='-', linewidth=1) # gridlines in graph

        plt.scatter(x, y, c=z, alpha=0.9) #show circle in every values with corresponding coordinates

        plt.ylabel("Depth",fontsize=30, color='k')
        plt.xlabel("Points Of Measurement" ,fontsize=30, color='k')

        contours = plt.contour(xi, yi, zi, 3, colors='k', linewidths = 1)
        plt.clabel(contours, inline=True, fontsize=24, colors='k')

        plt.title("WATER DETECTOR PLOTTER", loc="center",  fontsize=36, color='k') #set title

        plt.imshow(zi,vmin=z.min(), vmax=z.max(),  extent=[x.min(), x.max(), y.min(), y.max()], origin='lower', cmap=plt.cm.jet, alpha=0.9)

        plt.minorticks_on()
        plt.clim(-z.min(), z.max()) #Set the color limits of the current image.
        plt.colorbar()
        plt.gca().invert_yaxis()


        plt.savefig(root.getAbsolutePath() + "/fig1.png", bbox_inches= 'tight', pad_inches=0)

        bitmap = BitmapFactory.decodeFile(root.getAbsolutePath() + "/fig1.png")
        self.findViewById(R.id.imageView).setImageBitmap(bitmap)

        self.findViewById(R.id.button).setOnClickListener(self)




    @Override(jvoid, [View])
    def onClick(self, v):
        datetime_object = datetime.datetime.now()
        date = datetime_object.strftime("%m%d%Y, %H:%M:%S")
        root = Environment.getExternalStorageDirectory()
        plt.savefig(root.getAbsolutePath() + "/CSVFiles/" + date + ".png", bbox_inches= 'tight', pad_inches=0)


