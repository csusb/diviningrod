Divining Rod is an application that takes data in CSV form and outputs the location and type of material underground in a graph. It is developed by the Divining Rod team in Dr. Concepcions CSE 455 class.


The final version for prototype 2 that the QA team should test is under the folder "Prototype 2 Final Version" and is labeled. 


The User Manual for Prototype 2 is in the Documentation folder, please read this to understand how to use the app.  


The Work Log located in the Documents folder contains all test cases that the team has performed so far, as well as all metrics gathered during the project.


NOTE: For future teams, we were told to add Firbase functionality after the SRS and SPMP were finalized, so these documents do not mention it.


Original Team:

Joseph Lord          - Project Manager

Arusyak Hovhannesyan - Assistant Project Manager

Ben Alexander        - Software Engineer

Ivan Zermeno         - Software Engineer

Hwanyi Lee           - Software Engineer

Jose Perez           - Software Engineer

Andrew Beechko       - Software Engineer

Miguel Cerna-Benitez - UI/UX Designer
