package com.example.diviningrodv16;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;

import ca.hss.heatmaplib.HeatMap;
import ca.hss.heatmaplib.HeatMapMarkerCallback;

public class graph extends AppCompatActivity {

    Button input;
    Button main;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        input = (Button) findViewById(R.id.ToInput);
        main = (Button) findViewById(R.id.ToMain);

        Intent intent= getIntent();

        //ArrayList<Double> listDouble = (ArrayList<Double>) getIntent().getSerializableExtra("arraylist");
        ArrayList<Double> Arrdistance = (ArrayList<Double>) intent.getSerializableExtra("distance");
        ArrayList<Integer> Arrlength = intent.getIntegerArrayListExtra("length");
        ArrayList<Integer> Arrresistance = intent.getIntegerArrayListExtra("resistance");

        for( int i = 0; i < 3; i++)
        {
            if(Arrdistance.get(i) != null || Arrlength.get(i) != null || Arrresistance.get(i) != null)
            {
                System.out.println("\n" + Arrdistance.get(i) + Arrlength.get(i) + Arrresistance.get(i));
            }
        }


        HeatMap heatMap = (HeatMap) findViewById(R.id.heatmap);

        heatMap.setMinimum(0.0);
        heatMap.setMaximum(100.0);
        heatMap.setRadius(1000);

        Map<Float, Integer> colors = new ArrayMap<>();
        colors.put(0.0f, 0xFF00AF1F);
        colors.put(1.0f, 0xFF424BF4);
        heatMap.setColorStops(colors);

        Random rand = new Random();
        for (int i = 0; i < 50; i++) {
            HeatMap.DataPoint point = new HeatMap.DataPoint(rand.nextFloat(), rand.nextFloat(), rand.nextDouble() * 100.0);
            heatMap.addData(point);
        }


        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), input.class));
            }
        });

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
}
