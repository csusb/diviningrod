package com.example.diviningrodv16;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class input extends AppCompatActivity {
    EditText distance;
    EditText depth;
    EditText resistance;
    Button collect;
    Button main;
    Button graph;
    List<Double> Arrdistance = new ArrayList<Double>();
    List<Integer> Arrlength = new ArrayList<Integer>();
    List<Integer> Arrresistance = new ArrayList<Integer>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        Arrdistance.add(1.0);
        Arrdistance.add(2.0);
        Arrdistance.add(3.0);
        Arrlength.add(1);
        Arrlength.add(2);
        Arrlength.add(3);
        Arrresistance.add(1);
        Arrresistance.add(2);
        Arrresistance.add(3);

        for( int i = 0; i < 3; i++)
        {
            if(Arrdistance.get(i) != null || Arrlength.get(i) != null || Arrresistance.get(i) != null)
            {
                System.out.println("\n" + Arrdistance.get(i) + Arrlength.get(i) + Arrresistance.get(i));
            }
        }

        distance = (EditText) findViewById(R.id.distance);
        depth = (EditText) findViewById(R.id.depth);
        resistance = (EditText) findViewById(R.id.resistance);
        collect = (Button) findViewById(R.id.collect);


        main = (Button) findViewById(R.id.ToMain);
        graph = (Button) findViewById(R.id.ToGraph);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), graph.class);
                intent.putExtra("distance", (ArrayList<Double>) Arrdistance);
                intent.putIntegerArrayListExtra("length", (ArrayList<Integer>) Arrlength);
                intent.putIntegerArrayListExtra("resistance", (ArrayList<Integer>) Arrresistance);
                startActivity(intent);
            }
        });

    }


}
