package com.example.diviningrodv17;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class input extends AppCompatActivity {
    EditText distance;
    EditText depth;
    EditText resistance;
    Button collect;
    Button main;
    Button graph;
    List<Float> Arrdistance = new ArrayList<>();
    List<Float> Arrdepth = new ArrayList<>();
    List<Float> Arrresistance = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        distance = (EditText) findViewById(R.id.distance);
        depth = (EditText) findViewById(R.id.depth);
        resistance = (EditText) findViewById(R.id.resistance);
        collect = (Button) findViewById(R.id.collect);
        main = (Button) findViewById(R.id.ToMain);
        graph = (Button) findViewById(R.id.ToGraph);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Arrdistance.get(0) == null)
                {
                    System.out.println("must enter values\n" );
                    Toast.makeText(getApplicationContext(), "must enter values", Toast.LENGTH_SHORT).show();
                    return;
                }
                else
                    {
                        Intent intent=new Intent(getApplicationContext(), graph.class);
                        intent.putExtra("distance", (ArrayList<Float>) Arrdistance);
                        intent.putExtra("depth", (ArrayList<Float>) Arrdepth);
                        intent.putExtra("resistance", (ArrayList<Float>) Arrresistance);
                        startActivity(intent);
                    }
            }
        });

        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float num1;
                Float num2;
                Float num3;
                num1 = Float.parseFloat(distance.getText().toString());
                num2 = Float.parseFloat(depth.getText().toString());
                num3 = Float.parseFloat(resistance.getText().toString());
                if (TextUtils.isEmpty(distance.getText().toString()) || TextUtils.isEmpty(depth.getText().toString())|| TextUtils.isEmpty(resistance.getText().toString())) {
                    System.out.println("must enter values\n" );
                    Toast.makeText(getApplicationContext(), "must enter values", Toast.LENGTH_SHORT).show();
                }
                else if(num1 >=26 || num1 <= 0)
                {
                    System.out.println("must enter values between 0 and 26\n" );
                    Toast.makeText(getApplicationContext(), "must enter values between 0 and 26 for distance", Toast.LENGTH_LONG).show();
                }

                else if(num2 >=100 || num2 <= 0)
                {
                    System.out.println("must enter values between 0 and 26\n" );
                    Toast.makeText(getApplicationContext(), "must enter values between 0 and 26 for depth", Toast.LENGTH_LONG).show();
                }
                else if(num3 >=1 || num3 <= 0)
                {
                    System.out.println("must enter values between 0 and 1 for resistance\n" );
                    Toast.makeText(getApplicationContext(), "must enter values between 0 and 1 for resistance", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Arrdistance.add(num1);
                    Arrdepth.add(num2);
                    Arrresistance.add(num3);
                    System.out.println("\n" + Arrdistance.get(0));
                }
            }
        });
    }


}
