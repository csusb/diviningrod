package com.example.diviningrodv22;

/*
Divining Rod Program Version 1_8

This program is designed to collect data from manually entered input or data provided from a .csv file

Input serves as the data collection and importing activity for the program.  For phase 1 it consists of a manual input section, graph button, input tools, and a import csv button
*/

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class input extends AppCompatActivity {
    //these three edit text lines initialize the field that takes data from the user
    //data includes distance, depth and resistance
    EditText distance;
    EditText depth;
    EditText resistance;

    //this button will collect the int values from the edit text fields
    Button collect;
    //this button will take the user back to the main page
    Button main;
    //this button will take the user to the graph page
    Button graph;
    //this button will import a csv file for the user to use
    Button importfile;

    Button files;

    String path;

    TextView textView;

    private static final int READ_REQUEST_CODE = 1;
    //these arrays store the data entered by the user
    List<Float> Arrdistance = new ArrayList<>();
    List<Float> Arrdepth = new ArrayList<>();
    List<Float> Arrresistance = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        //these linse initializes the buttons and edit text fields from the xml file
        distance = (EditText) findViewById(R.id.distance);
        depth = (EditText) findViewById(R.id.depth);
        resistance = (EditText) findViewById(R.id.resistance);
        collect = (Button) findViewById(R.id.collect);
        main = (Button) findViewById(R.id.ToMain);
        graph = (Button) findViewById(R.id.ToGraph);
        importfile = (Button) findViewById(R.id.importfile);

        files = (Button) findViewById(R.id.filing);


        //this line sets the listener.  whenever the main button is pressed the program will change the activity to the main activity
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        //this line sets the listener.  whenever the graph button is pressed the program will change the activity to the graph activity
        //the program will also collecet data stored in arrays and send it in packets to the graph activity
        //this will only happen if data is present in all three arrays and not null in any array
        graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if a data array is empty then the input page will refuse to let the user draw the graph as this could throw an error
                if( Arrdistance.size() == 0 || Arrresistance.size() == 0 || Arrdepth.size() == 0)
                {
                    System.out.println("must enter values\n" );
                    Toast.makeText(getApplicationContext(), "must enter values", Toast.LENGTH_SHORT).show();
                    return;
                }
                //if arrays have values then the arrays are sent to the graph activity and the activity is changed from input to graph
                else
                {
                    //this line prepares packets to be sent to the graph class
                    Intent intent=new Intent(getApplicationContext(), graph.class);

                    //these next three lines puts all three arrays in a bundle and sends them to the graph activity
                    intent.putExtra("distance", (ArrayList<Float>) Arrdistance);
                    intent.putExtra("depth", (ArrayList<Float>) Arrdepth);
                    intent.putExtra("resistance", (ArrayList<Float>) Arrresistance);

                    //this line  starts the graph activity specified above
                    startActivity(intent);
                }
            }
        });
        //this line sets the listener.  whenever the collect button is pressed the program will collect data input into the edit text fields and store them in their corresponding arrays
        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //these lines initiate float variables for temporary storage
                Float num1;
                Float num2;
                Float num3;

                //this statements will read if any of the three edit text fields are empty.  if they are then the program return and will not allow user to collect the data
                if (TextUtils.isEmpty(distance.getText().toString()) || TextUtils.isEmpty(depth.getText().toString())|| TextUtils.isEmpty(resistance.getText().toString())) {
                    System.out.println("must enter values\n" );

                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    Toast.makeText(getApplicationContext(), "must enter values", Toast.LENGTH_SHORT).show();
                    return;
                }

                //these next three lines will save the usres inputed data to the temporary variables.  this must be done so the program can make sure the data is in the accepted range
                num1 = Float.parseFloat(distance.getText().toString());
                num2 = Float.parseFloat(depth.getText().toString());
                num3 = Float.parseFloat(resistance.getText().toString());

                //this will check to see if the distance value was entered correctly.  If it was not then no input will be collected
                if(num1 >26 || num1 < 0)
                {
                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    System.out.println("must enter values between 0 and 26\n" );
                    Toast.makeText(getApplicationContext(), "must enter values between 0 and 26 for distance", Toast.LENGTH_LONG).show();
                    return;
                }

                //this will check to see if the depth value was entered correctly.  If it was not then no input will be collected
                else if(num2 >100 || num2 < 0)
                {
                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    System.out.println("must enter values between 0 and 100\n" );
                    Toast.makeText(getApplicationContext(), "must enter values between 0 and 100 for depth", Toast.LENGTH_LONG).show();
                    return;
                }

                //this will check to see if the resistance value was entered correctly.  If it was not then no input will be collected
                else if(num3 >=1 || num3 <= 0)
                {
                    //this line will display a floating text onto the UI to show the user the correct way to input the data
                    System.out.println("must enter values below 1 and above 0 for resistance\n" );
                    Toast.makeText(getApplicationContext(), "must enter values below 1 and above 0 for resistance", Toast.LENGTH_LONG).show();
                    return;
                }

                //If all conditions are satisfied then the program will save the corresponding data into their corresponding arrays
                else
                {
                    Arrdistance.add(num1);
                    Arrdepth.add(num2);
                    Arrresistance.add(num3);
                }
            }
        });

        importfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Scanner scan = new Scanner(getResources().openRawResource(R.raw.data));
                ArrayList<String[]> records = new ArrayList<>();
                String[] record;
                ArrayList<Float> tempdistance = new ArrayList<>();
                ArrayList<Float> tempdepth = new ArrayList<>();
                boolean count = true;

                while(scan.hasNext())
                {
                    record = scan.nextLine().split(",");
                    if(!count)
                    {
                        for (int i = 1; i < record.length; i++)
                        {
                            if(TextUtils.isEmpty(record[i]))
                            {
                                Arrresistance.add((float) 0);
                            }
                            else
                            {
                                Arrresistance.add(Float.valueOf(record[i]));
                            }
                        }
                        if(TextUtils.isEmpty(record[0]))
                        {
                            tempdepth.add((float) 0);
                        }
                        else
                        {
                            tempdepth.add(Float.valueOf(record[0]));
                        }
                    }

                    if(count)
                    {
                        for (int i = 1; i < record.length; i++)
                        {
                            if(TextUtils.isEmpty(record[i]))
                            {
                                tempdistance.add((float) 0);
                            }
                            else
                            {
                                tempdistance.add(Float.valueOf(record[i]));
                            }
                        }
                        count = false;
                    }
                    records.add(record);
                }

                for(int i = 0; i < tempdepth.size(); i++)
                {
                    for(int k = 0; k < tempdistance.size(); k++)
                    {
                        Arrdistance.add(tempdistance.get(k));//stores 14 values each time
                        Arrdepth.add(tempdepth.get(i));//happens 14 times
                    }
                }

                for( int i = 0; i < Arrdepth.size(); i++)
                {

                    if(Arrdistance.get(i) != null || Arrdepth.get(i) != null || Arrresistance.get(i) != null)
                    {
                        System.out.println("\n" + Arrdepth.get(i) + ", " + Arrdistance.get(i) + ", " + Arrresistance.get(i));
                    }
                }
            }
        });

        files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent,READ_REQUEST_CODE );
            }
        });



    }

}

