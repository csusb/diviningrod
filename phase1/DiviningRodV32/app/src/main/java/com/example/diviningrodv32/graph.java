package com.example.diviningrodv32;

/*
Divining Rod Program Version 1_8

This program is designed to collect data from manually entered input or data provided from a .csv file

Graph serves as the graph creation page for the program.  For phase 1 it consists of the graph view, input button, and main button
*/

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;

import ca.hss.heatmaplib.HeatMap;
import ca.hss.heatmaplib.HeatMapMarkerCallback;

public class graph extends AppCompatActivity {

    //this button will take the user back to the input page
    Button input;

    //this button will take the user back to the main page
    Button main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        //these linse initialize the buttons from the xml file
        input = (Button) findViewById(R.id.ToInput);
        main = (Button) findViewById(R.id.ToMain);

        //these lines will recieve the array data from the input acvitiy and store them in duplicate arrays
        Intent intent = getIntent();
        ArrayList<Float> Arrdistance = (ArrayList<Float>) intent.getSerializableExtra("distance");
        ArrayList<Float> Arrdepth = (ArrayList<Float>) intent.getSerializableExtra("depth");
        ArrayList<Float> Arrresistance = (ArrayList<Float>) intent.getSerializableExtra("resistance");

        //this button initializes the array placeholder from the xml file
        HeatMap heatMap = (HeatMap) findViewById(R.id.heatmap);

        //these lines set boundaries and radius for the graph
        heatMap.setMinimum(0.0);
        heatMap.setMaximum(100.0);
        heatMap.setRadius(1000);



        //these next four lines define the color of the data points in the graph.
        //for this graph colors are defined as gradients.
        //The gtradient chosen is from green to blue as defined below
        Map<Float, Integer> colors = new ArrayMap<>();
        colors.put(0.0f, 0xFF00AF1F);
        colors.put(1.0f, 0xFF424BF4);
        heatMap.setColorStops(colors);

        //this statement will take information from the arrays recieved from the input activity and draw the graph using the data
        for (int i = 0; i < Arrdistance.size(); i++) {
            if(Arrresistance.get(i)*100 < 30 && Arrresistance.get(i)*100 > 10)
            {
                HeatMap.DataPoint point = new HeatMap.DataPoint((Arrdistance.get(i)/26), (Arrdepth.get(i)/100), Arrresistance.get(i)*100);
                heatMap.addData(point);
            }

        }

        //this line helps developers see what values are stored in the arrays and will not be visible by users.
        //it can be viewed from the logcat section or from the debugger section of android studio
        for( int i = 0; i < Arrdistance.size(); i++)
        {
            if(Arrdistance.get(i) != null || Arrdepth.get(i) != null || Arrresistance.get(i) != null)
            {
                System.out.println("\n" + Arrdistance.get(i) + Arrdepth.get(i) + Arrresistance.get(i) * 100);
            }
        }

        //this line sets the listener.  whenever the input button is pressed the program will change the activity to the input activity
        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), input.class));
            }
        });

        //this line sets the listener.  whenever the main button is pressed the program will change the activity to the main activity
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });
    }
}
