package com.example.heatgraphv2;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FragmentA extends Fragment {
    private static final String Tag = "FragmentA";

    private Button btnNavFragB;
    EditText distance;
    EditText depth;
    EditText resistance;
    Button collect;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmenta, container, false);

        distance = (EditText) view.findViewById(R.id.distance);
        depth = (EditText) view.findViewById(R.id.depth);
        resistance = (EditText) view.findViewById(R.id.resistance);
        collect = (Button) view.findViewById(R.id.collect);

        collect.setOnClickListener(new View.OnClickListener(){
        @Override
        public void onClick(View v) {



        }
    });

        btnNavFragB = (Button) view.findViewById(R.id.btnNavFragB);
        Log.d(Tag,"onCreateView: started.");

        btnNavFragB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setViewPager(1);

            }
        });
        return view;
    }
}