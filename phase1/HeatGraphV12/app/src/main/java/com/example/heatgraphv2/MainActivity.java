package com.example.heatgraphv2;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Random;

import ca.hss.heatmaplib.HeatMap;
import ca.hss.heatmaplib.HeatMapMarkerCallback;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private SectionStatePager mSectionStatePager;
    private ViewPager mViewPager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

    }

    private void setupViewPager(ViewPager viewPager){
        SectionStatePager adapter = new SectionStatePager(getSupportFragmentManager());
        adapter.addFragment(new FragmentA(), "FragmentA");
        adapter.addFragment(new FragmentB(), "FragmentB");
        viewPager.setAdapter(adapter);

    }
    public void setViewPager(int fragmentnumber){
        mViewPager.setCurrentItem(fragmentnumber);
    }
}
