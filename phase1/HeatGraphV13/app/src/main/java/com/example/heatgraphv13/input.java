package com.example.heatgraphv13;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class input extends AppCompatActivity {
    EditText distance;
    EditText depth;
    EditText resistance;
    Button collect;
    Button main;
    Button graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        distance = (EditText) findViewById(R.id.distance);
        depth = (EditText) findViewById(R.id.depth);
        resistance = (EditText) findViewById(R.id.resistance);
        collect = (Button) findViewById(R.id.collect);


        main = (Button) findViewById(R.id.ToMain);
        graph = (Button) findViewById(R.id.ToGraph);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), graph.class));
            }
        });

    }


}
